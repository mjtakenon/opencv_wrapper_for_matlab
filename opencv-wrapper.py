import cv2
import numpy as np


def warp_perspective_4points(src_quad, dst_quad, img):
    """
    4組の点と画像を入力、射影変換を行い画像を出力

    Parameters
    ----------
    src_quad: np.ndarray
        1枚目の画像の特徴点 shape=(4,) np.float32
    dst_quad: np.ndarray
        2枚目の画像の特徴点 shape=(4,) np.float32
    img: np.ndarray
        2枚目の画像 shape=(y_size, x_size, color) np.uint8

    Returns
    -------
    warpimg: np.ndarray
        射影変換後画像 shape=(y_size, x_size, color) np.uint8
    """

    src_quad = np.asarray(src_quad).astype(np.float32)
    dst_quad = np.asarray(dst_quad).astype(np.float32)
    img = np.asarray(img).astype(np.uint8)
    M = cv2.getPerspectiveTransform(src_quad, dst_quad)
    warpimg = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]))
    return warpimg


def warp_perspective_ransac(src, dst, img):
    """
    n組の点と画像を入力、RANSACでアウトライアを除去し射影変換を行い画像を出力

    Parameters
    ----------
    src: np.ndarray
        1枚目の画像の特徴点 shape=(n,) np.float32
    dst: np.ndarray
        2枚目の画像の特徴点 shape=(n,) np.float32
    img: np.ndarray
        2枚目の画像 shape=(y_size, x_size, color) np.uint8

    Returns
    -------
    warpimg: np.ndarray
        射影変換後画像 shape=(y_size, x_size, color) np.uint8
    """

    src = np.asarray(src).astype(np.float32)
    dst = np.asarray(dst).astype(np.float32)
    img = np.asarray(img).astype(np.uint8)
    M, mask = cv2.findHomography(src, dst, cv2.RANSAC, 5.0)
    warpimg = cv2.warpPerspective(img, M, (img.shape[1], img.shape[0]))
    return warpimg


def warp_perspective(perspective_matrix, img):
    """
    射影変換行列を入力、射影変換を行い同じサイズの画像を出力

    Parameters
    ----------
    perspective_matrix: np.ndarray
        射影変換行列 shape=(3, 3) np.float32
    img: np.ndarray
        入力画像 shape=(y_size, x_size, color) np.uint8

    Returns
    -------
    warpimg: np.ndarray
        射影変換後画像 shape=(y_size, x_size, color) np.uint8
    """

    perspective_matrix = np.asarray(perspective_matrix).astype(np.float64)
    img = np.asarray(img).astype(np.uint8)
    warpimg = cv2.warpPerspective(img, perspective_matrix, (img.shape[1], img.shape[0]))
    return warpimg


def get_homography_matrix(src, dst):
    """
    n組の点を入力, RANSACでアウトライアを除去し射影変換行列を算出

    Parameters
    ----------
    src: np.ndarray
        1枚目の画像の特徴点 shape=(n,) np.float32
    dst: np.ndarray
        2枚目の画像の特徴点 shape=(n,) np.float32

    Returns
    -------
    M: np.ndarray
        射影変換行列 shape=(3, 3) np.float32
    """

    src = np.asarray(src).astype(np.float32)
    dst = np.asarray(dst).astype(np.float32)
    M, mask = cv2.findHomography(src, dst, cv2.RANSAC, 5.0)
    return M


def get_perspective_transform_matrix_4points(src_quad, dst_quad):
    """
    4組の点を入力し、射影変換行列を出力 (RANSAC無し/高速)

    Parameters
    ----------
    src_quad: np.ndarray
        1枚目の画像の特徴点 shape=(4,) np.float32
    dst_quad: np.ndarray
        1枚目の画像の特徴点 shape=(4,) np.float32

    Returns
    -------
    M: np.ndarray
        射影変換行列 shape=(3, 3) np.float32
    """

    src_quad = np.asarray(src_quad).astype(np.float32)
    dst_quad = np.asarray(dst_quad).astype(np.float32)
    M = cv2.getPerspectiveTransform(src_quad, dst_quad)
    return M


def get_contours(img):
    """
    画像を入力、しきい値処理して回転を考慮した外接矩形の4点を出力

    Parameters
    ----------
    img: np.ndarray
        入力画像 shape=(y_size, x_size, color) np.uint8

    Returns
    -------
    box: np.ndarray
        射影変換行列 shape= np.int32
    """

    img = np.asarray(img).astype(np.uint8)
    ret, thresh = cv2.threshold(img, 127, 255, 0)
    # opencv3.*
    # imgEdge,contours,hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    # opencv4.* >=
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnt = contours[0]
    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    return box


def get_convex_hull(img):
    """
    画像を入力、凸包を出力

    Parameters
    ----------
    img: np.ndarray
        入力画像 shape=(y_size, x_size, color) np.uint8

    Returns
    -------
    box: np.ndarray
        射影変換行列 shape= np.int32
    """

    img = np.asarray(img).astype(np.uint8)
    ret, thresh = cv2.threshold(img, 127, 255, 0)
    edge, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnt = contours[0]
    hull = cv2.convexHull(cnt)
    return hull

